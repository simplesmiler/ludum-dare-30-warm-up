local SGraphics = Klass()

function SGraphics:draw(roster)
  local start = roster:queryId("start")
  local start_x, start_y = unpack(start.position)

  local trackpoints = roster:queryTag("trackpoint")
  for _, trackpoint in ipairs(trackpoints) do
    local x, y = unpack(trackpoint.position)
    if (trackpoint.active) then
      love.graphics.circle("fill", x, y, 8, 5)

      love.graphics.line(trackpoint.bezier_segment:render(4))
    end
  end

end

return SGraphics