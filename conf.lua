local conf = {
  title = "Base Code",
  width = 960,
  height = 540
}

function love.conf(t)
  t.version = "0.9.1"
  t.window.title = conf.title
  t.window.width = conf.width
  t.window.height = conf.height
  t.window.resizable = false
  t.window.fullscreen = false
  t.window.vsync = true
end

return conf