local SInput = Klass()

function SInput:init()
  -- self._keys = {}
  self._mouse = {
    position = {}
  }
end

function SInput:keypressed(key, isrepeat)
  -- assert(false, "SInput:keypressed not implemented")
end

function SInput:keyreleased(key)
  -- assert(false, "SInput:keyreleased not implemented")
end

function SInput:mousepressed(x, y, button)
  -- assert(false, "SInput:mousepressed not implemented")
end

function SInput:mousereleased(x, y, button)
  -- assert(false, "SInput:mousereleased not implemented")
end

function SInput:mousemove(x, y)
  self._mouse.position = {x, y}
  -- assert(false, "SInput:mousemove not implemented")
end

function SInput:update(roster, dt)
  -- TODO: do stuff ;D
end


return SInput