local Roster = Klass()

function Roster:init(EntityKlass)
  self._EntityKlass = EntityKlass
  self._entities = {}
end

function Roster:add(entity)
  -- TODO: check if entity is already added
  self._entities[entity.id] = entity
end

function Roster:create(...)
  local entity = self._EntityKlass(...)
  self:add(entity)
end

-- TODO: implement :remove(entity) or :remove(id) ?

function Roster:queryId(qid)
  for id, entity in pairs(self._entities) do
    if entity.id == qid then
      return entity
    end
  end
end

function Roster:queryTag(tag)
  local entities = {}

  for id, entity in pairs(self._entities) do
    if entity:hasTag(tag) then
      table.insert(entities, entity)
    end
  end

  return entities
end

function Roster:queryTags(tags)
  local entities = {}

  for id, entity in pairs(self._entities) do
    if entity:hasTags(tags) then
      table.insert(entities, entity)
    end
  end

  return entities
end

return Roster