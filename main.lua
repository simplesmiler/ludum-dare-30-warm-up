-- global
Klass = require("lib/Klass")
love.math.setRandomSeed(12, 1000)

-- subsystems
SPhysics = require("SPhysics")
SInput = require("SInput")
SLogics = require("SLogics")
SGraphics = require("SGraphics")

Entity = require("Entity")
Roster = require("Roster")

pieces = require("pieces")

-- main
shared = {
  sys = {},
  res = {},
  roster = nil -- in love.load
}

function love.load()
  shared.res.flake = love.graphics.newImage("assets/flake.png")
  shared.sys.particles = love.graphics.newParticleSystem(shared.res.flake, 500)

  shared.sys.particles:setEmissionRate(100)
  shared.sys.particles:setEmitterLifetime(-1)
  shared.sys.particles:setParticleLifetime(5)
  shared.sys.particles:setPosition(480, 270)
  shared.sys.particles:setSpread(3.1415 / 8)
  shared.sys.particles:setDirection(3.1415)
  shared.sys.particles:setSpeed(200)
  shared.sys.particles:setSizes(0.2, 0.3, 0.8, 0.5)
  shared.sys.particles:setSizeVariation(0.1)
  shared.sys.particles:setSpin(3.1415 / 2, -6.283 / 2)
  shared.sys.particles:setSpinVariation(1)
  shared.sys.particles:setColors(-1, -1, -1, 255,   -1, -1, -1, 0)
  shared.sys.particles:setLinearAcceleration(-20, -20, 0, -20)
  shared.sys.particles:start()

  shared.sys.physics = SPhysics()
  shared.sys.logics = SLogics()
  shared.sys.input = SInput()
  shared.sys.graphics = SGraphics()

  shared.roster = Roster(Entity)
  shared.roster:create("player", {}, {
    pieces.position(480, 540),
    pieces.speed(0, 0),
    pieces.script(function (self, dt)
      self.position[1] = love.mouse.getX()
      self.position[2] = love.mouse.getY()
    end)
  })
  shared.roster:create("start", {}, {
    pieces.position(960, 270),
    pieces.speed(-240, 0)
  })
  for x = 1, 6 do
    shared.roster:create("trackpoint" .. x, { "trackpoint" }, {
      pieces.position((x - 1) * 960 / 4, love.math.random(500) + 20),
      pieces.speed(-240, 0),
      pieces.active(false),
      pieces.bezier_segment(),
      pieces.script(function (self, dt, roster)
        if (self.position[1] < - 960 / 4) then
          self.position[1] = 960 + 960 / 4
          self.position[2] = love.math.random(500) + 20
          self.bezier_segment = shared.sys.logics:constructSegment(roster, self)
          self.active = true
        end 
      end)
    })
  end
end

local accum = 0
local step = 1.0 / 60.0

function love.update(dt)
  accum = accum + dt

  while (accum > step) do
    mx, my = love.mouse.getPosition()
    shared.sys.input:mousemove(mx, my)

    -- update subsystems
    shared.sys.physics:update(shared.roster, step)
    shared.sys.logics:update(shared.roster, step)
    shared.sys.input:update(shared.roster, step)

    local player = shared.roster:queryId("player")
    shared.sys.particles:moveTo(unpack(player.position))
    shared.sys.particles:update(step)

    accum = accum - step
  end
end

function love.draw()
  love.graphics.setBackgroundColor(20, 20, 28)

  love.graphics.draw(shared.sys.particles, dx, dy)
  shared.sys.graphics:draw(shared.roster)
end

function love.keypressed(key, isrepeat)
  shared.sys.input:keypressed(key, isrepeat)
end

function love.keyreleased(key)
  shared.sys.input:keyreleased(key)
end

function love.mousepressed(x, y, button)
  shared.sys.input:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
  shared.sys.input:mousereleased(x, y, button)
end