local Entity = Klass()

function Entity:init(id, extratags, pieces)
  extratags = extratags or {}
  pieces = pieces or {}

  self.id = id
  self.tags = {}

  for id, piece in ipairs(pieces) do
    piece(self)
  end

  for id, tag in ipairs(extratags) do
    self.tags[tag] = true
  end
end

function Entity:hasTag(tag)
  return self.tags[tag]
end

function Entity:hasTags(tags)
  for _, tag in pairs(tags) do
    if not self.tags[tag] then
      return false
    end
  end

  return true
end

-- TODO: implement :hasAnyTag

return Entity