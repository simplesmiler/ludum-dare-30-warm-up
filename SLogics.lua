local SLogics = Klass()

function SLogics:init()
  -- body
end

function SLogics:update(roster, dt)
  local entities = roster:queryTag("pieces.script")

  for _, entity in pairs(entities) do
    entity:script(dt, roster)
  end


  local trackpoints = roster:queryTag("trackpoint")
  local player = roster:queryId("player")
  local px, py = unpack(player.position)

  -- from smaller to bigger
  table.sort(trackpoints, function (p1, p2)
    return p1.position[1] < p2.position[1]
  end)

  local segment = nil
  local approx = 0
  for id, trackpoint in ipairs(trackpoints) do
    local tx, ty = unpack(trackpoint.position)
    if tx > px then
      ptx, pty = unpack(trackpoints[id-1].position) -- DANGEROUS?
      if (tx - ptx) > math.abs(ty - pty) then
        approx = 1 - (tx - px) / (tx - ptx)
      else
        approx = 1 - (ty - py) / (ty - pty)
      end

      segment = trackpoint.bezier_segment
      break
    end
  end

  if segment ~= nil then
    approx = math.max(0, math.min(approx, 1))
    local projx, projy = segment:evaluate(approx)
  end


end

function SLogics:constructSegment(roster, trackpoint)
  local trackpoints = roster:queryTag("trackpoint")

  table.sort(trackpoints, function (p1, p2)
    return p1.position[1] > p2.position[1]
  end)

  local x1, y1 = unpack(trackpoints[2].position)
  local x2, y2 = unpack(trackpoint.position)

  local segment = love.math.newBezierCurve({
    x1,       y1,
    x1 + 120, y1,
    x2 - 120, y2,
    x2,       y2
  })

  return segment
end

return SLogics