local SPhysics = Klass()

function SPhysics:update(roster, dt)
  local entities = roster:queryTags({"pieces.position", "pieces.speed"})

  for _, entity in ipairs(entities) do
    x, y = unpack(entity.position)
    sx, sy = unpack(entity.speed)

    nx = x + sx * dt
    ny = y + sy * dt

    entity.position = {nx, ny}

    if entity:hasTag("pieces.bezier_segment") then
      if entity.bezier_segment ~= nil then
        entity.bezier_segment:translate(sx * dt, sy * dt)
      end
    end
  end
end

return SPhysics