return function (mode)
  if mode == nil then mode = "strict" end

  local magic = {} -- metatable for klass instances
  magic.__index = magic

  local function constructor (klass, ...)
    local base = setmetatable({}, magic)
    if base.init then base:init(...) end
    return base
  end

  return setmetatable(magic, { __call = constructor })
end