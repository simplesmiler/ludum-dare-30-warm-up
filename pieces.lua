local pieces = {}

function pieces.position(x, y)
  assert(x and y, "position should be specified")

  return function (entity)
    entity.position = {x, y}
    entity.tags["pieces.position"] = true
  end
end

function pieces.speed(sx, sy)
  assert(sx and sy, "speed should be specified")

  return function (entity)
    entity.speed = {sx, sy}
    entity.tags["pieces.speed"] = true
  end
end

function pieces.script(script)
  assert(script, "script should be specified")

  return function (entity)
    entity.script = script
    entity.tags["pieces.script"] = true
  end
end

function pieces.active(defl)
  assert(defl == true or defl == false, "default value should be specified")

  return function (entity)
    entity.active = defl
    entity.tags["pieces.active"] = true
  end
end

function pieces.bezier_segment()
  return function (entity)
    entity.bezier_segment = nil
    entity.tags["pieces.bezier_segment"] = true
  end
end



return pieces